
'use strict';

// gulp plugins
var gulp     = require('gulp'),
gutil        = require('gulp-util'),
es           = require('event-stream'),
stylus       = require('gulp-stylus'),
autoprefixer = require('gulp-autoprefixer'),
jshint       = require('gulp-jshint'),
clean        = require('gulp-clean'),
connect      = require('gulp-connect'),
browserify   = require('gulp-browserify'),
usemin       = require('gulp-usemin'),
imagemin     = require('gulp-imagemin'),
rename       = require('gulp-rename');

// Connect Task

gulp.task('connect', function(){
  connect.server({
    root: ['../tbl/'],
    port: 3000,
    livereload: true
  });
});
// Html reload
gulp.task('html', function () {
  return gulp.src('./app/**/*.html')
    .pipe(connect.reload());
});

// stylus compiler task
gulp.task('stylus', function () {
  return gulp.src('./app/css/stylus/main.styl')
    .pipe(stylus({
      onError: function (error) {
        gutil.log(gutil.colors.red(error));
        gutil.beep();
      },
      onSuccess: function () {
        gutil.log(gutil.colors.green('Stylus compiled successfully.'));
      }
    }))
    .pipe(autoprefixer('last 2 version','ios 6', 'android 4'))
    .pipe(gulp.dest('./app/css/'))
    .pipe(connect.reload());
});

// Minify img
gulp.task('imagemin', function () {
  return gulp.src('app/img/*')
        .pipe(imagemin({
            progressive: true
        }))
        .pipe(gulp.dest('dist/img'));

});

gulp.task('js',function () {
  return gulp.src('app/js/app.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(browserify({
      insertGlobals: true
    }))
    .pipe(rename(function (path) {
      path.basename = 'bundle';
    }))
    .pipe(gulp.dest('app/js'))
    .pipe(connect.reload());
});


gulp.task('watch', function () {
  gulp.watch([ 'app/css/**/*.styl'], ['stylus']);
  gulp.watch([ 'app/js' + '/**/*.js'], ['js']);
  gulp.watch(['./app/**/*.html'], ['html']);
});


gulp.task('default', ['connect', 'stylus', 'js', 'watch']);


gulp.task('usemin', function () {
  return gulp.src('./app/*.html')
    .pipe(usemin())
    .pipe(gulp.dest('./dist'));
});

gulp.task('clean-build', function () {
  return gulp.src('dist/', {read: false})
    .pipe(clean());
});

gulp.task('folder', function(){
  return gulp.src('./app/template/*/*.html')
    .pipe(gulp.dest('./dist/template'));
});

gulp.task('build', ['clean-build', 'stylus', 'js', 'usemin', 'imagemin', 'folder'], function () {
});

